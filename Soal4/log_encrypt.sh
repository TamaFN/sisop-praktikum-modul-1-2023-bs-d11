#!/bin/bash

# membuat file enkripsi sesuai dengan format jam:menit tanggal:bulan:tahun (dalam format .txt)
encrypt_file="$(date +'%H:%M %d-%m-%Y').txt"
# membuat variabel hour yang berisi jam saat ini
hour="$(date +'%H')"
# variabel hour menggunakan format 24 jam sehingga ada kemungkinan jam ditulis 01-09
# maka variabel shift digunakan untuk menghapus angka 0 di depan jam
shift="${hour#0}"

# melakukan loop untuk membaca file yang akan di enkripsi
# -n1 menyatakan bahwa program akan membaca setiap 1 karakter yang ada di file
while read -r -n1 char
do
    # convert setiap input huruf ke dalam bentuk ASCII
    ascii_char=$(printf '%d' "'$char'")
    # jika char merupakan huruf kapital
    if [[ $char =~ [A-Z] ]]
    then 
        # maka 
        # 1) ascii_char - 65 --> 65 adalah ASCII dari huruf A, untuk mengubah range code dari 65-90(ASCII A-Z) menjadi 0-25
        # 2) + shift --> menjalankan metode cipher
        # 3) % 26 --> di modulo 26 untuk memastikan hasilnya tidak lebih dari range 0-25
        # 4) + 65 --> agar hasilnya kembali ke dalam bentuk ASCII uppercase
        encrypt_uppercase=$(( ("$ascii_char" - 65 + "$shift")%26 + 65 ))
        # mengganti huruf pada variabel char dengan huruf yang sudah dienkripsi
        char=$(printf '%b' "$(printf '\\x%x' "$encrypt_uppercase")")
    #jika char merupakan huruf kecil    
    elif [[ $char =~ [a-z] ]] 
    then
        # maka 
        # 1) ascii_char - 97 --> 97 adalah ASCII dari huruf a, untuk mengubah range code dari 97-112(ASCII a-z) menjadi 0-25
        # 2) + shift --> menjalankan metode cipher
        # 3) % 26 --> di modulo 26 untuk memastikan hasilnya tidak lebih dari range 0-25
        # 4) + 65 --> agar hasilnya kembali ke dalam bentuk ASCII lowercase
        encrypt_lowercase=$(( ("$ascii_char" - 97 + "$shift")%26 + 97 ))
        char=$(printf '%b' "$(printf '\\x%x' "$encrypt_lowercase")")
    fi
    #print char sebagai string
    printf '%s' "$char"
#file yang ingin dienkrisi berada pada directory /var/log/syslog dan akan di simpan pada file "$encrypt_file" 
done < /var/log/syslog > /home/sarahnrhsna/Documents/Soal4/encrypt_file/"$encrypt_file"

# cron jobs:
# crontab -e
# 0 */2 * * * /home/sarahnrhsna/Documents/Soal4/encrypt_file/log_encrypt.sh

# referensi
# https://www.ibm.com/docs/en/sdse/6.4.0?topic=configuration-ascii-characters-from-33-126