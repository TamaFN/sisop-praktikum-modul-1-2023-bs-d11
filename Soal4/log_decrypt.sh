#!/bin/bash

#masukkan input nama file yang ingin di deskripsi
echo "Type the filename:"
read -r filename

decrypt_file="$(date +'%H:%M %d-%m-%Y')_decrypted-$filename"
# jam yang digunakan adalah jam pada file enkripsi sehingga harus melakukan cut simulai dari simbol ":" agar hasil yang didapat hanya bagian hour saja
hour="$(echo "$filename" | cut -d ':' -f 1)"
shift="${hour#0}"

while read -r -n1 char
do
    ascii_char=$(printf '%d' "'$char'")
    #ketika char merupakan huruf kapital
    if [[ $char =~ [A-Z] ]]
    then 
        decrypt_uppercase=$(( ("$ascii_char" - 65 - "$shift" + 26 )% 26 + 65  ))
        char=$(printf '%b' "$(printf '\\x%x' "$decrypt_uppercase")")
    #ketika char merupakan huruf kecil
    elif [[ $char =~ [a-z] ]] 
    then
        decrypt_lowercase=$(( ("$ascii_char" - 97 - "$shift" + 26 )% 26 + 97 ))
        char=$(printf '%b' "$(printf '\\x%x' "$decrypt_lowercase")")
    fi
    printf '%s' "$char"
done < /home/sarahnrhsna/Documents/Soal4/encrypt_file/"$filename" > /home/sarahnrhsna/Documents/Soal4/decrypt_file/"$decrypt_file"

# referensi
# https://ciksiti.com/id/chapters/3652-bash-cut-command-with-examples--linux-hint 