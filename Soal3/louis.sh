#!/bin/bash

date=$(date +'%Y-%m-%d %H:%M:%S')

echo -e "\nREGISTER SYSTEM"
echo "Input username:"
read -r username

echo "Input password:"
read  -r password

#fungsi untuk memvalidasi password
validate_password(){
	password=$1

	#password minimal 8 karakter
	if [ ${#password} -lt 8 ]
	then
		echo "REGISTER: ERROR Failed password length should be at least 8 characters"
		return 1
	fi

	#password hanya alphanumeric
	#jika ada simbol maka
	if ! echo "$password" | grep -q '^[[:alnum:]]*$'
	then
		echo "REGISTER: ERROR Failed password should contain alphanumeric only"
		return 1
	fi

	#password minimal 1 huruf kapital, 1 huruf kecil, dan 1 angka
	if ! [[ $password =~ [A-Z] ]] || ! [[ $password =~ [a-z] ]] || ! [[ $password =~ [0-9] ]]
	then
    		echo "REGISTER: ERROR Failed password should contain at least one uppercase letter, one lowercase letter, and one digit number"
		return 1
	fi

	return 0
}

#Cek apakah username sudah pernah ada di /users/users.txt
if grep -q "$username" /users/users.txt
then
	echo "REGISTER: ERROR User already exists"
	echo "Please try again"
	echo "$date REGISTER: ERROR User already exists" >> log.txt
else
	#jika tidak maka
	#cek apakah password = username
	if [ "$password" == "$username" ]
	then
		echo "REGISTER: ERROR Failed password cannot be the same as username"
		echo "Please try again"
	else
		#cek apakah password = chicken
		if [ "$password" == "chicken" ]
		then
			echo "REGISTER: ERROR Failed you can't use 'chicken' as your password"
			echo "Please try again"
		else
			#cek apakah password = ernie
			if [ "$password" == "ernie" ]
			then
				echo "REGISTER: ERROR Failed you can't use 'ernie' as your password"
				echo "Please try again"
			else
				#cek validasi password menggunakan fungsi validate_password
				if ! validate_password "$password"
				then
					echo "Please try again"
				else
					#REGISTER BERHASIL
					echo "REGISTER: INFO User $username registered successfully"
					echo "$date REGISTER: INFO User $username registered successfully" >> log.txt
					echo "username: $username, password: $password" >> /users/users.txt
				fi
			fi
		fi
	fi
fi

#referensi:
#https://superuser.com/questions/1603567/how-to-verify-if-a-shell-script-variable-is-alphanumeric
#https://www.cyberciti.biz/faq/linux-unix-formatting-dates-for-display/
#https://www.geekinterview.com/question_details/91266#:~:text=Write%20a%20script%20to%20validate,password%20as%20%22weak%20password%22.

