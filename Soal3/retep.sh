#!/bin/bash

date=$(date +'%Y-%m-%d %H:%M:%S')

echo "Hello! :3"
echo "Do you have an account?y/n?"
echo -n "Answer: "
read -r answer

#jika user menjawab n maka user akan langsung menuju REGISTER SYSTEM
if [ "$answer" == n ]
then
	bash louis.sh
else
	echo -e "\nLOGIN SYSTEM"
	echo "Please input your username:"
	read -r username

	echo "Please input your password:"
	read -r -s password

	#Cek kembali apakah user benar-benar sudah memiliki akun
	Check_Username=$(awk '/'"$username"'/ {print 1}' /users/users.txt)
	if [[ $Check_Username -ne 1 ]]
	then
		echo "LOGIN: ERROR Failed you haven't registered yet/wrong username"
		echo "$date LOGIN: ERROR Failed login attempt on user $username" >> log.txt

		echo -e "\nGo to register system? y/n?"
		echo -n "Answer: "
		read -r command

		if [ "$command" == y ]
		then
			bash louis.sh
		else
			exit
		fi
	else
		#Cek apakah password sesuai
        	if grep -q "$username" /users/users.txt && grep -q "$password" /users/users.txt
        	then
			#LOGIN BERHASIL
                	echo "LOGIN: INFO User $username logged in"
                	echo "$date LOGIN: INFO User $username logged in" >> log.txt
        	else
                	echo "LOGIN: ERROR Failed login attempt on user $username"
                	echo "$date LOGIN: ERROR Failed login attempt on user $username" >> log.txt
		fi
	fi
fi
