#!/bin/bash

# Soal No 1 

# Poin 1 
# Tampilkan 5 Universitas dengan rangking tertinggi di Jepang

# Untuk dapat menampilkan 5 universitas tertinggi, bisa menggunakan syntax berikut

grep Japan university.csv | sort -k 1 -n |head -n 5

# Poin 2
# Cari Faculty Student Score (FSR Score) yang paling rendah diantara 5 Universitas di Jepang

# Untuk dapat mencari 5 Universitas dengan FSR Score terendah dapat menggunakan syntax berikut

grep Japan university.csv | sort -n -t ';' -k 9 | head -n 5

# Poin 3
# Cari 10 Universitas di Jepang dengan Employment Outcome Rank (ger rank) paling tinggi

# Untuk dapat mencari 10 Universitas jepang dengan ger rank paling tinggi, dapat menggunakan syntax berikut

grep Japan university.csv | sort -n -t ';' -k 20 | head -n 10

# Poin 4
# Cari universitas dengan kata kunci "Keren"

# Untuk mencari universitas dengan kata kunci "Keren", dapat menggunakan syntax berikut

grep 'Keren' university.csv




